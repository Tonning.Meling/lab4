package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  public int rows;
  public int cols;
  CellPosition cellPosition = null;
  public Color[][] grid;

  public ColorGrid (int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new Color[rows][cols];
  }
  //initialiserer felt-variablene
  @Override
  public Color get(CellPosition pos){
    if (pos.row() >= 0 && pos.row() < rows && pos.col() >= 0 && pos.col() < cols) {
      return grid[pos.row()][pos.col()]; // Returnerer fargen på den angitte posisjonen
  } else {
      throw new IndexOutOfBoundsException("Position is out bounds");
    }
  }
  @Override
  public void set(CellPosition pos, Color color){
    if (pos.row() >= 0 && pos.row() < rows && pos.col() >= 0 && pos.col() < cols) {
      grid[pos.row()][pos.col()] = color; // Setter fargen på den angitte posisjonen
    } else {
      throw new IndexOutOfBoundsException("Position is out bounds");
    }
  }
  // Du kan legge til håndtering for posisjoner utenfor rutenettet om nødvendig
  @Override
  public int cols() {
    // Implementerer cols
    return cols;
  }
  @Override
  public int rows() {
    // Implementerer rows
    return rows;
  }
  @Override
  public List<CellColor> getCells(){
    List<CellColor> cellColors = new ArrayList<>(); // Correctly initialize the list
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            CellPosition position = new CellPosition(row, col);
            Color color = grid[row][col]; // This assumes your grid is initialized and possibly modified
            cellColors.add(new CellColor(position, color)); // Add CellColor object to list
        }
    }
    return cellColors;
  }
}