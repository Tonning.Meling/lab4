package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import javax.swing.JPanel;

import java.awt.geom.Rectangle2D;
import java.util.List;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

public class GridView extends JPanel {
    // Holder dataen for farge-griden
    IColorGrid colorGrid;

    // Konstanterer margin og bakgrunnsfarge 30 og lysegrå.
    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

    // Konstruktør, setter størrelse og grid data
    public GridView(IColorGrid colorGrid){
        this.setPreferredSize(new Dimension(400, 300));
        this.colorGrid = colorGrid;
    }

    // Tegner komponenten som kalles på  av Swing
    //@Override
    public void paintComponent(Graphics g){
        super.paintComponent(g); //les mer på superfunksjonen
        Graphics2D g2 = (Graphics2D) g;


        
        // Tegner gridet
        drawGrid(g2);
    }

    // Tegner gridet med gitt grafikkontekst
    private void drawGrid(Graphics2D graphics2d){
        // Konverterer celleposisjon til piksler
        CellPositionToPixelConverter cellPositionToPixelConverter = new CellPositionToPixelConverter(getBounds(), colorGrid, OUTERMARGIN);

        // Tegner cellene i gridet
        drawCells(graphics2d, colorGrid, cellPositionToPixelConverter);
    }

    // Tegner cellene basert på farge og posisjon
    private static void drawCells(Graphics2D graphics2d, CellColorCollection cellColorCollection, CellPositionToPixelConverter cellPositionToPixelConverter){ // :))))) Endelig!!!!!!!!!
        // Henter liste over celler
        List<CellColor> cells = cellColorCollection.getCells();

        // Går gjennom hver celle og tegner den
        for (int i = 0; i < cells.size(); i++) {
            CellColor cell = cells.get(i);

            // Setter farge, standard mørke grå hvis ingen annen farge
            graphics2d.setColor(cell.color() == null ? Color.DARK_GRAY : cell.color());

            // Henter celleposisjon og beregner genser
            CellPosition cellPosition = cell.cellPosition();
            Rectangle2D cellBounds = cellPositionToPixelConverter.getBoundsForCell(cellPosition);

            // Fyller celle med farge!
            graphics2d.fill(cellBounds);
        }
    }
}