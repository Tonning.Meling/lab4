package no.uib.inf101.gridview;

import java.awt.Color;
import javax.swing.JFrame;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

public class Main {
    public static void main(String[] args) {
        // Opprettet ColorGrid med 3 rader og 4 kolonner.
        IColorGrid colorGrid = new ColorGrid(3, 4);

        // Sett fargene i hjørnene
        colorGrid.set(new CellPosition(0, 0), Color.RED);   // Rød i hjørnet oppe til venstre
        colorGrid.set(new CellPosition(0, 3), Color.BLUE);  // Blå i hjørnet oppe til høyre
        colorGrid.set(new CellPosition(2, 0), Color.YELLOW); // Gul i hjørnet nede til venstre
        colorGrid.set(new CellPosition(2, 3), Color.GREEN); // Grønn i hjørnet nede til høyre

        // Opprettet GridView med ColorGrid.
        GridView canvas = new GridView(colorGrid);

        // Oppretter og konfigurererererer JFrame.
        JFrame frame = new JFrame("INF101");
        frame.setContentPane(canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}